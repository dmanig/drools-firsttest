package org.manigdni.tests.droolstest;

import org.kie.api.KieServices;
import org.kie.api.builder.*;
import org.kie.api.runtime.KieContainer;
import org.kie.internal.io.ResourceFactory;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class RuleHolder {

    private Set<String> files = new HashSet<>();
    private KieServices kieServices = KieServices.Factory.get();
    private KieContainer kieContainer;

    @PostConstruct
    public void init() {
        files.add("measurment-telegram");
        files.add("myfirstrule");
        createKieContainer();
    }

    public void addFile(String fileName) {
        files.add(fileName);
        try {
            createKieContainer();
        } catch (Exception e) {
            files.remove(fileName);
            throw e;
        }
    }

    private void createKieContainer() {
        KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
        for (String file : files) {
            kieFileSystem.write(ResourceFactory.newClassPathResource("rules/" + file + ".drl"));
        }

        KieBuilder kb = kieServices.newKieBuilder(kieFileSystem);
        kb.buildAll();
        if (kb.getResults().hasMessages(Message.Level.ERROR)) {
            throw new RuntimeException("Build Errors:\n" + kb.getResults().toString());
        }
        KieModule kieModule = kb.getKieModule();
        this.kieContainer = kieServices.newKieContainer(kieModule.getReleaseId());
    }

    public KieContainer kieContainer() {
        if (this.kieContainer == null) {
            createKieContainer();
        }
        return this.kieContainer;
    }

    private void getKieRepository() {
        final KieRepository kieRepository = kieServices.getRepository();
        kieRepository.addKieModule(new KieModule() {
            public ReleaseId getReleaseId() {
                return kieRepository.getDefaultReleaseId();
            }
        });
    }

}
