package org.manigdni.tests.droolstest.applicant;

import org.manigdni.tests.droolstest.RuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;

public class ApplicantWebService {
    @Autowired
    private RuleService as;

    @RequestMapping("/role")
    public String getRole(@RequestParam int age, @RequestParam double currentSalary, @RequestParam int experienceInYears) throws IOException {
        long startTime = System.currentTimeMillis();
        Applicant a = new Applicant("David", age, currentSalary, experienceInYears);
        SuggestedRole sr = new SuggestedRole();
        as.suggestARoleForApplicant(a, sr);
        return sr.getRole() + " - needed time: " + (System.currentTimeMillis() - startTime) + "ms";
    }
}
