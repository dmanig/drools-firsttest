package org.manigdni.tests.droolstest.dpsconnect;

import org.apache.commons.lang.time.StopWatch;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.runtime.KieContainer;
import org.kie.internal.io.ResourceFactory;
import org.manigdni.tests.droolstest.RuleHolder;
import org.manigdni.tests.droolstest.dpsconnect.parts.Part;
import org.manigdni.tests.droolstest.dpsconnect.telegrams.MeasurementTelegram;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class DpsConnect {


    @Autowired
    private PartService partService;

    @Autowired
    private RuleHolder ruleHolder;


    @RequestMapping("/measurement/{dmc}/{machine}")
    public Part measurement(@PathVariable("dmc") String dmc, @PathVariable("machine") String machine, @RequestParam String name, @RequestParam double value) {
        MeasurementTelegram telegram = MeasurementTelegram.builder().dmc(dmc).machine(machine).name(name).value(value).build();
        StopWatch sw = new StopWatch();
        sw.start();

        Part p = partService.addMeasurement(telegram);

        sw.stop();

        System.out.println("needed time: " + sw.toString());
        return p;

    }

    @RequestMapping("/activateFile/{name}")
    public String activateFile(@PathVariable("name") String name) {
        ruleHolder.addFile(name);

        return "successfully added";
    }

}
