package org.manigdni.tests.droolstest;

import org.drools.core.base.RuleNameEndsWithAgendaFilter;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.AgendaFilter;
import org.manigdni.tests.droolstest.applicant.Applicant;
import org.manigdni.tests.droolstest.applicant.SuggestedRole;
import org.manigdni.tests.droolstest.dpsconnect.parts.Part;
import org.manigdni.tests.droolstest.dpsconnect.telegrams.MeasurementTelegram;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class RuleService {
    @Autowired
    private RuleHolder ruleHolder;

    public SuggestedRole suggestARoleForApplicant(Applicant applicant, SuggestedRole suggestedRole) {
        KieSession kieSession = ruleHolder.kieContainer().newKieSession();
        kieSession.insert(applicant);
        kieSession.setGlobal("suggestedRole", suggestedRole);
        kieSession.fireAllRules();
        return suggestedRole;

    }

    public Part processRulesForMeasurement(Part part, MeasurementTelegram telegram) {
        KieSession kieSession = ruleHolder.kieContainer().newKieSession();
        kieSession.insert( telegram);
        kieSession.insert( part);
        kieSession.fireAllRules(new RuleNameEndsWithAgendaFilter("MeasurementTelegramRule::"+telegram.getMachine()));
        System.out.println("processRulesForMeasurement");
        return part;

    }
}