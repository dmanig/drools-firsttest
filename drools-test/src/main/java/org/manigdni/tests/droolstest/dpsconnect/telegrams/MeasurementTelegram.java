package org.manigdni.tests.droolstest.dpsconnect.telegrams;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MeasurementTelegram {
    private String dmc;
    private String machine;
    private double value;
    private String name;
}
