package org.manigdni.tests.droolstest.dpsconnect;

import org.manigdni.tests.droolstest.dpsconnect.parts.Part;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class DpsConnectData {
    private Map<String, Part> data = new HashMap<>();

    public Optional<Part> get(String dmc) {
        return Optional.ofNullable(data.get(dmc));
    }

    public void set(Part part) {
        Optional.of(part).map(Part::getDmc).ifPresent(p -> data.put(part.getDmc(), part));
    }
}
