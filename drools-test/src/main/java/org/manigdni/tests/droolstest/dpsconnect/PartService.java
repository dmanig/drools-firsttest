package org.manigdni.tests.droolstest.dpsconnect;

import org.manigdni.tests.droolstest.RuleService;
import org.manigdni.tests.droolstest.dpsconnect.DpsConnectData;
import org.manigdni.tests.droolstest.dpsconnect.parts.Part;
import org.manigdni.tests.droolstest.dpsconnect.telegrams.MeasurementTelegram;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PartService {
    @Autowired
    private DpsConnectData dpsConnectData;

    @Autowired
    private RuleService ruleService;

    public Part addMeasurement(MeasurementTelegram telegram) {
        Part p = dpsConnectData.get(telegram.getDmc()).orElse(Part.of(telegram.getDmc()));
        ruleService.processRulesForMeasurement(p, telegram);
        p.addMeasurement(telegram);
        dpsConnectData.set(p);
        return p;
    }
}
